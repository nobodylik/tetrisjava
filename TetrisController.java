import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


public class TetrisController
{
    private TetrisView theView;
    private TetrisModel theModel;
    public TimerTask timerTask;
    public Timer timer;

    int speed;

    public TetrisController(TetrisView theView, TetrisModel theModel) {

        this.theView = theView;
        this.theModel = theModel;
        this.theView.addExitButtonListener(new ExitListener());
        this.theView.addAboutButtonListener(new AboutListener());
        this.theView.addHighScoresButtonListener(new HighScoreListener());
        this.theView.addNewGameButtonListener(new NewGameListener());
        this.theView.addRotateButtonListener(new RotateListener());
        this.theView.addLeftButtonListener(new LeftListener());
        this.theView.addRightButtonListener(new RightListener());
        this.theView.addDownButtonListener(new DownListener());
        this.theView.addTetrisArea(theModel);
        this.theView.addDropDownButtonListener(new DropDownListener());
        this.theView.addPauseButtonListener(new PauseListener());
        this.theView.addContinueButtonListener(new ContinueListener());

        this.theView.addKeyListener(new java.awt.event.KeyAdapter()
        {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt)
            {
                KeyReleased(evt);
            }
        });
        this.theView.setFocusable(true);    //To make the keys active
        this.theView.setVisible(true);


        speed =1;
        timerTask = new MyTimerTask();

        timer = new Timer(true);
        // будем запускать каждых 10 секунд (10 * 1000 миллисекунд)
        timer.scheduleAtFixedRate(timerTask, 0, 1*100);
    }


    public void ChangeSpeed()
    {
int s = theModel.score;
        if(s<1000)
            speed =1;
        else if(s>1000)
            speed =2;
        else if(s>5000)
            speed =3;
        else if(s>10000)
            speed =4;
        else if(s>15000)
            speed =5;
        else if(s>20000)
            speed =6;
        else if(s>25000)
            speed =7;
        else if(s>30000)
            speed =8;
        else if(s>35000)
            speed =9;


    }


    public void KeyReleased(java.awt.event.KeyEvent e) {

        if(!theModel.GameActive)
            return;

        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP:
                this.goRotate();
                break;
            case KeyEvent.VK_DOWN:
                this.goDown();
                break;
            case KeyEvent.VK_LEFT:
                this.goLeft();
                break;
            case KeyEvent.VK_RIGHT:
                this.goRight();
                break;
            case KeyEvent.VK_SPACE:
                this.dropDown();
                break;
        }
    }


    public void dropDown()
    {

        theModel.GameActive = false; // блокируем работу таймера
        while(theModel.Down()) {


    //        theView.bUseClipOnNextPaint = true;
    //        theView.repaint();


     //       theView.bUseClipOnNextPaint = true;
     //       theView.paint(theView.getGraphics());
            try {
                Thread.sleep(10);
            }
            catch (InterruptedException e1){}
        }
        theModel.CreateNextFigure();

        theModel.GameActive = true; // воссстанавливаем работу таймера

        theView.repaint();
    }


    public void goRight()
    {
        theModel.Right();
        theView.repaint();
    }

    public void goLeft()
    {
        theModel.Left();
        theView.repaint();
    }

    public void goRotate()
    {
        theModel.Rotate();
        theView.repaint();
    }

    public void goDown()
    {
        if(!theModel.Down())
            theModel.CreateNextFigure();
        theView.repaint();

        theView.repaint();
    }

    class ExitListener implements ActionListener
        {
            public void actionPerformed(ActionEvent e)
                {
                    System.exit(0);
                }
        }


    class AboutListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            JOptionPane.showMessageDialog(null, "Эту программу Tetris написал Шестак Антон, г17210");
        }

    }

    class HighScoreListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {

            String m="";

            for(int i=0; i<theModel.ResultTable.size(); i++)
                m+=i+1+". "+theModel.ResultTable.get(i).name+": "+theModel.ResultTable.get(i).score+"\n";

            JOptionPane.showMessageDialog(null, m);
        }

    }

    class NewGameListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            theModel.EndGame();
            String n =JOptionPane.showInputDialog("name:");
            theView.FPlayerName.setText(n);
            theModel.plname = n;

            theModel.CreateNextFigure();
            theModel.GameActive= true;
        }

    }

    class ContinueListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            theModel.GameActive = true;
        }
    }

    class PauseListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            theModel.GameActive = false;
        }

    }


    class RotateListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
           if(theModel.GameActive)
               goRotate();
        }

    }

    class LeftListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if(theModel.GameActive) goLeft();
        }

    }


    class RightListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if(theModel.GameActive) goRight();
        }
    }

    class DropDownListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if(theModel.GameActive)
                dropDown();
        }
    }

    class DownListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if(theModel.GameActive)
                goDown();
        }

    }

    public class MyTimerTask extends TimerTask {
        int p=0;
        @Override
        public void run()
        {
            ChangeSpeed();
            p++;
            if(p==10-speed)
            {
                if (theModel.GameActive)
                    goDown();
                p=0;
            }


         }
    }


}