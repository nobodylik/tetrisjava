public class Main
{
    public static void main(String[] args) {
        TetrisView theView = new TetrisView();

        TetrisModel theModel = new TetrisModel();

        TetrisController theController = new TetrisController(theView, theModel);

        theView.setVisible(true);
    }
}
