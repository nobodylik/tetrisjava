import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class TetrisView extends JFrame
{
    TetrisModel W; // данные для рисования
    boolean bUseClipOnNextPaint; // флаг для рисования

    public JTextField FPlayerName  = new JTextField(10);
    private JTextField FScore  = new JTextField(10);
    private JLabel LPlayerName  = new JLabel("Player:", JLabel.LEFT);
    private JLabel LPlayerScore  = new JLabel("Score:", JLabel.LEFT);
    private JButton BExit = new JButton("exit");
    private JButton BAbout = new JButton("about");
    private JButton BHighScore = new JButton("high scores");
    private JButton BNewGame = new JButton("new game");
    private JButton BRotate = new JButton("Rotate");
    private JButton Bleft = new JButton("left");
    private JButton Bright = new JButton("right");
    private JButton Bdown = new JButton("down");
    private JButton BDropDown = new JButton("dropdown");
    private JButton BPause = new JButton("pause");
    private JButton BContinue = new JButton("continue");


    TetrisView()
    {
        bUseClipOnNextPaint = false;
        JPanel TetrisPanel = new JPanel();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(540, 1000);
        this.setResizable(false);

        TetrisPanel.setLayout(null);

        BContinue.setFocusable(false);
        BPause.setFocusable(false);
        BDropDown.setFocusable(false);
        Bdown.setFocusable(false);
        BRotate.setFocusable(false);
        Bleft.setFocusable(false);
        Bright.setFocusable(false);
        BNewGame.setFocusable(false);
        BHighScore.setFocusable(false);
        BAbout.setFocusable(false);
        FPlayerName.setFocusable(false);
        FScore.setFocusable(false);

        BDropDown.setBounds(120+58,95,120,30);
        Bdown.setBounds(310+58,95,60,30);
        BRotate.setBounds(310+58,63,60,30);
        BPause.setBounds(120+58,63,60,30);
        BContinue.setBounds(180+61,63,60,30);
        Bleft.setBounds(248+58,95,60,30);
        Bright.setBounds(430,95,60,30);

        BNewGame.setBounds(20,20,110,40);
        BHighScore.setBounds(140,20,110,40);
        BAbout.setBounds(260,20,110,40);
        BExit.setBounds(380,20,110,40);

        LPlayerName.setBounds(20, 70, 60, 20);
        FPlayerName.setBounds(65, 70, 110, 20);

        LPlayerScore.setBounds(20, 95, 60, 20);
        FScore.setBounds(65, 95, 110, 20);

        FPlayerName.setText("Player 1");
        FScore.setText("0");

        TetrisPanel.add(BExit);
        TetrisPanel.add(LPlayerName);
        TetrisPanel.add(BAbout);
        TetrisPanel.add(BHighScore);
        TetrisPanel.add(BNewGame);
        TetrisPanel.add(FPlayerName);
        TetrisPanel.add(FScore);
        TetrisPanel.add(LPlayerScore);

        TetrisPanel.add(BContinue);
        TetrisPanel.add(BPause);
        TetrisPanel.add(BRotate);
        TetrisPanel.add(Bleft);
        TetrisPanel.add(Bright);
        TetrisPanel.add(Bdown);
        TetrisPanel.add(BDropDown);

        add(TetrisPanel);
    }

    public String getName()
    {
        return (FPlayerName.getText());
    }

    void addTetrisArea(TetrisModel arr)
    {
        W = arr;
    }

    void addExitButtonListener(ActionListener listenForButton){
        BExit.addActionListener(listenForButton);
    }
    void addAboutButtonListener(ActionListener listenForButton){
        BAbout.addActionListener(listenForButton);
    }

    void addHighScoresButtonListener(ActionListener listenForButton){
        BHighScore.addActionListener(listenForButton);
    }

    void addNewGameButtonListener(ActionListener listenForButton){
        BNewGame.addActionListener(listenForButton);
    }

    void addRotateButtonListener(ActionListener listenForButton){
        BRotate.addActionListener(listenForButton);
    }

    void addLeftButtonListener(ActionListener listenForButton){
        Bleft.addActionListener(listenForButton);
    }

    void addRightButtonListener(ActionListener listenForButton){
        Bright.addActionListener(listenForButton);
    }

    void addDownButtonListener(ActionListener listenForButton){
        Bdown.addActionListener(listenForButton);
    }

    void addDropDownButtonListener(ActionListener listenForButton){
        BDropDown.addActionListener(listenForButton);
    }

    void addPauseButtonListener(ActionListener listenForButton){
        BPause.addActionListener(listenForButton);
    }

    void addContinueButtonListener(ActionListener listenForButton){
        BContinue.addActionListener(listenForButton);
    }



    Color getFigureColor(int c)
    {
        switch (c)
        {
            case 1:
            {
                return Color.red;
            }
            case 2:
            {
                return Color.YELLOW;
            }
            case 3:
            {
                return Color.MAGENTA;
            }
            case 4:
            {
                return Color.green;
            }
            case 5:
            {
                return Color.cyan;
            }
            case 6:
            {
                return Color.blue;
            }
            case 7:
            {
                return Color.darkGray;
            }
        }
return Color.BLACK;
    }



    public void paint(Graphics g)
    {

        super.paint(g);



        int x = 70;
        int y = 155;

        int step = 40; // размеры квадратика


        /*
        if (bUseClipOnNextPaint)
        {
           super.paint(g);
           int xx = (W.F.F_X-5) * step + x;
           int yy = (W.F.F_Y-5) * step + y;
           g.setClip(xx, yy, 6 * step, 6 * step);
       }
        else
        bUseClipOnNextPaint= false;
        */

        Color oldColor = g.getColor();
        g.setColor(Color.BLACK);

        for (int i =5; i<15; i++)
        {
            for (int j =5; j<25; j++)
                if(W.A1[i][j]>0)
                {

                    g.setColor(getFigureColor(W.A1[i][j]));
                    g.fillRoundRect(x+(i-5)*step,y+(j-5)*step,step, step, 20, 15);
                    g.setColor(Color.BLACK);
                    g.drawRoundRect(x+(i-5)*step,y+(j-5)*step,step, step, 20, 15);

                }

        }
        g.drawRect(x,y,10*step, 20*step);

      // g.fillRect(70, 155, 400, 800);
       g.setColor(oldColor);
            FScore.setText(Integer.toString (W.score));

   }








}
