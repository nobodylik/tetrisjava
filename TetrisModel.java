import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;



public class TetrisModel {


    ArrayList<oneResult> ResultTable = new ArrayList<oneResult>();
//    TetrisArea Window = new TetrisArea();
    int[][] A1 = new int[20][30];
    int[][] A2 = new int[20][30];
    String plname;
    int score=0;
    boolean GameActive = false;

    Figura F ;

    TetrisModel()
    {
        /*//A1[12][15]=1;
        AddRes("marina" ,5);
        AddRes("papa" ,2);
        AddRes("anton" ,100);*/

    }

    public class oneResult
    {
        public String name;
        public int score;
        public oneResult(String N, int S)
        {
            name = N;
            score = S;
        }
    }



    public void AddRes(String name, int score)
    {
        oneResult r = new oneResult(name,score);

        if(ResultTable.size()==0) {
            ResultTable.add(r);
            return;
        }
        for (int i = 0; i < ResultTable.size(); i++)
            if (ResultTable.get(i).score < r.score)
            {
                ResultTable.add(i, r);
                return;
            }

        ResultTable.add(r);

    }


    boolean DrawShape(int Symb, int[][] Ar)
    {

        int [][] nowFigura = F.GetShape();

        for (int i =0; i<5; i++)
        {
            for (int j =0; j<5; j++)
            {
                if (nowFigura[i][j] >0)
                    if((Symb>0)&&(Ar[i+F.F_X][j+F.F_Y]>0))
                        return false;
                    else if((i+F.F_X<5)||(i+F.F_X>14 ))
                        return false;
                    else if((j+F.F_Y<5)||(j+F.F_Y>24))
                        return false;
                    else
                        Ar[i+F.F_X][j+F.F_Y]=Symb;

            }

        }
        return true;
    }

    private void Copy(int[][] x, int[][] y)
    {
        for (int i =0; i<20; i++)
        {
            for (int j =0; j<30; j++)
                y[i][j] = x[i][j];
        }
    }





    public boolean Rotate()
    {
        Copy(A1, A2);
        DrawShape(0, A2);
        F.rotateRight();

        if(!DrawShape(F.FigureID, A2))
        {
            F.rotateLeft();
            return false;
        }


        Copy(A2, A1);

        return true;
    }

    public boolean Left()
    {
        Copy(A1, A2);
        DrawShape(0, A2);
        F.left();

        if(!DrawShape(F.FigureID, A2))
        {
            F.right();
            return false;
        }

        Copy(A2, A1);

        return true;
    }

    public boolean Right()
    {
        Copy(A1, A2);
        DrawShape(0, A2);
        F.right();

        if(!DrawShape(F.FigureID, A2))
        {
            F.left();
            return false;
        }


        Copy(A2, A1);

        return true;
    }

    public boolean Down()
    {
        Copy(A1, A2);
        DrawShape(0, A2);
        F.down();

        if(!DrawShape(F.FigureID, A2))
        {
            F.up();
            return false;
        }


        Copy(A2, A1);

        return true;
    }

    public boolean CreateNextFigure()
    {
        int k = CheckFullLines();

        switch (k)
        {
            case 1: {
                score+=100;
                break;
            }
            case 2: {
                score+=300;
                break;
            }
            case 3: {
                score += 700;
                break;
            }
            case 4: {
                score+=1500;
                break;
            }
        }



        F = new Figura(0);
        if(DrawShape(F.FigureID, A1))
            return true;
        else
        {
            EndGame();
            return false;
        }

    }


    public int CheckFullLines()
    {
        int k=0;
        for (int j=5; j<25; j++)
        {
            A1[4][j]=11;
            for (int i=5; i<15; i++) {
                if (A1[i][j] ==0) {
                    A1[4][j] = -11;
                    break;
                }
            }
            if(A1[4][j]==11)
                k++;
        }

        if(k==0)
            return k;

        for (int j=5; j<25; j++)
            for (int i = 5; i < 15; i++)
                A2[i][j] = 0;

        int j2=24; // строка во 2ой матрице
        for (int j1=24; j1>4; j1--)
        {
            if(A1[4][j1]!=11) {
                for (int i = 5; i < 15; i++) {
                    //копируем строку
                    A2[i][j2] = A1[i][j1];
                }
                j2--;
            }
        }
        Copy(A2, A1);

        return k;
    }

public void Clear()
{
    for (int i =0; i<20; i++)
        for (int j =0; j<30; j++)
            A1[i][j]=0;

}

    public void EndGame()
    {

        GameActive = false;
        Clear();
        if(score!=0)
           AddRes( plname,score);
        score=0;
        GameActive = false;

    }




}
