import java.util.Random;







public class Figura
{

    int[][] roll = new int[5][5]; //1 - x, 2 - y
    int PosCount;
    int rotationPos=0;
    public int F_X=7;
    public int F_Y=4;

int FigureID;

Figura(int i)
{

    if(i==0)
    {
        int max =7;
        int min =1;
        Random rnd = new Random(System.currentTimeMillis());
        i = min + rnd.nextInt(max - min + 1);
    }

FigureID=i;
   switch(i)
   {
       case 1:
       {
           roll[2][1] = 1;      ////
           roll[3][1] = 1;      //
           roll[2][2] = 1;      //
           roll[2][3] = 1;


           PosCount = 4;
           break;
       }
       case 2:
       {
           roll[2][1] = 2;    ////
           roll[1][1] = 2;      //
           roll[2][2] = 2;      //
           roll[2][3] = 2;

           PosCount = 4;
           break;
       }
       case 3:
       {
           roll[1][2] = 3;       //
           roll[2][1] = 3;    //////
           roll[2][2] = 3;
           roll[3][2] = 3;

           PosCount = 4;
           break;
       }
       case 4:
       {
           roll[1][2] = 4;       ////
           roll[2][1] = 4;    ////
           roll[2][2] = 4;
           roll[3][1] = 4;

           PosCount = 2;
           break;
       }
       case 5:
       {
           roll[1][1] = 5;    ////
           roll[2][1] = 5;      ////
           roll[2][2] = 5;
           roll[3][2] = 5;

           PosCount = 2;
           break;
       }
       case 6:
       {
           roll[2][1] = 6;      //
           roll[2][2] = 6;      //
           roll[2][3] = 6;      //
           roll[2][4] = 6;      //

           PosCount = 2;
           break;
       }
       case 7:
       {
           roll[2][1] = 7;      ////
           roll[3][1] = 7;      ////
           roll[2][2] = 7;
           roll[3][2] = 7;

           PosCount = 1;
           break;
       }

   }
}



   public void rotateRight()
    {
        rotationPos--;
        rotationPos = (rotationPos +4) %PosCount;

    }
    public void rotateLeft()
    {
        rotationPos++;
        rotationPos = (rotationPos +4) %PosCount;

    }


    public void up()
    {
        F_Y --;
    }

    public void down()
    {
        F_Y ++;
    }

    public void left()
    {
        //if(F_X>0)
        F_X --;
    }

    public void right()
    {
        F_X ++;
    }



    public int[][] GetShape()
    {
        int[][] M = new int[5][5];


        switch (rotationPos)
        {
            case 0: {
                for (int i = 0; i < 5; i++) {
                    for (int j = 0; j < 5; j++)
                        M[i][j] = roll[i][j];
                }
                break;
            }
            case 1:
            {
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 5; j++)
                        M[j][4 - i] = roll[i][j];
                }
                break;
            }
            case 2:
            {
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 5; j++)
                        M[4-i][4-j] = roll[i][j];
                }
                break;
            }
            case 3:
            {
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 5; j++)
                        M[4 - j][i] = roll[i][j];
                }
                break;
            }
            default:
                break;
        }
        return M;
    }
}
